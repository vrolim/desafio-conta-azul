
# coding: utf-8

# In[1]:


get_ipython().run_line_magic('matplotlib', 'inline')

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from lifelines import KaplanMeierFitter


# In[2]:


customers = pd.read_csv("dados/sub.csv")

sns.set(palette = "colorblind", font_scale = 1.35, 
        rc = {"figure.figsize": (12,9), "axes.facecolor": ".92"})


# In[3]:


customers.head()


# In[4]:


kmf = KaplanMeierFitter() 


# In[5]:


kmf.fit(durations = customers.tenure, 
        event_observed = customers.inactive)


# In[6]:


kmf.plot()
plt.title("Estimativa Kaplan-Meier para inscritos (1958-2006)")
plt.ylabel("Probabilidade de que um clinete ainda esteja inscrito")

plt.show()


# In[11]:


kmf_plano = KaplanMeierFitter()

tenure = customers.tenure
observed = customers.inactive 
planos = ["Bottom", "Middle", "Top"]

fig, axes = plt.subplots(nrows = 1, ncols=3, sharey = True, figsize=(10,5))

for plano, ax in zip(planos, axes.flatten()):
    idx = customers.rate_plan == plano
    kmf_plano.fit(tenure[idx], observed[idx])
    kmf_plano.plot(ax=ax, legend=False)
    ax.annotate("Mediana =\n {:.0f} dias".format(kmf_plano.median_), xy = (.47, .5), 
                xycoords = "axes fraction", fontsize=12)
    ax.set_xlabel("")
    ax.set_title(plano)
    ax.set_xlim(0,50000)
    ax.set_ylim(0,1)

fig.tight_layout()

fig.text(0.5, -0.01, "Timeline (Dias)", ha="center")

fig.text(-0.05, 0.5, "Probabilidade de que um cliente\n ainda esteja inscrito", 
         va="center", rotation="vertical")

fig.suptitle("Curva de sobrevivência de inscrições por tipo de plano (1958-2006)",
             fontsize=20)

fig.subplots_adjust(top=0.7)

plt.show()


# In[9]:


kmf_cidade = KaplanMeierFitter()

tenure = customers.tenure
observed = customers.inactive

planos = ["Metropolis", "Smallville", "Gotham"]

fig, axes = plt.subplots(nrows = 1, ncols=3, sharey = True, figsize=(10,5))

for plano, ax in zip(planos, axes.flatten()):
    idx = customers.market == plano
    kmf_cidade.fit(tenure[idx], observed[idx])
    kmf_cidade.plot(ax=ax, legend=False)
    ax.annotate("Mediana =\n {:.0f} dias".format(kmf_cidade.median_), xy = (.47, .5), 
                xycoords = "axes fraction")
    ax.set_xlabel("")
    ax.set_title(plano)
    ax.set_xlim(0,5000)
    ax.set_ylim(0,1)

fig.tight_layout()

fig.text(0.5, -0.01, "Timeline (Dias)", ha="center")

fig.text(-0.05, 0.5, "Probabilidade de que um cliente\n ainda esteja inscrito", 
         va="center", rotation="vertical")

fig.suptitle("Curva de sobrevivência de inscrições por mercado (1958-2006)",
             fontsize=20)

fig.subplots_adjust(top=0.7)

plt.show()


# In[10]:


kmf_canal = KaplanMeierFitter()

tenure = customers.tenure
observed = customers.inactive

planos = ["Mail", "Store", "Chain", "Dealer"]

fig, axes = plt.subplots(nrows = 1, ncols=4, sharey = True, figsize=(10,5))

for plano, ax in zip(planos, axes.flatten()):
    idx = customers.channel == plano
    kmf_canal.fit(tenure[idx], observed[idx])
    kmf_canal.plot(ax=ax, legend=False)
    ax.annotate("Mediana =\n {:.0f} dias".format(kmf_canal.median_), xy = (.47, .5), 
                xycoords = "axes fraction")
    ax.set_xlabel("")
    ax.set_title(plano)
    ax.set_xlim(0,5000)
    ax.set_ylim(0,1)

fig.tight_layout()

fig.text(0.5, -0.01, "Timeline (Dias)", ha="center")

fig.text(-0.05, 0.5, "Probabilidade de que um cliente\n ainda esteja inscrito", 
         va="center", rotation="vertical")

fig.suptitle("Curva de sobrevivência de inscrições por canal (1958-2006)",
             fontsize=20)

fig.subplots_adjust(top=0.7)

plt.show()


# In[ ]:


customers5000 = pd.read_csv("dados/sub5000.csv")


# In[ ]:


kmf5000 = KaplanMeierFitter() 


# In[ ]:


kmf5000.fit(durations = customers5000.tenure, 
        event_observed = customers5000.inactive)


# In[ ]:


kmf5000.plot()
plt.title("Estimativa Kaplan-Meier para inscritos (1958-2006) até 5000 dias de duração")
plt.ylabel("Probabilidade de usuário ainda inscrito")

plt.show()


# In[ ]:


kmf_plano = KaplanMeierFitter()

tenure = customers5000.tenure
observed = customers5000.inactive

planos = ["Bottom", "Middle", "Top"]

fig, axes = plt.subplots(nrows = 1, ncols=3, sharey = True, figsize=(10,5))

for plano, ax in zip(planos, axes.flatten()):
    idx = customers5000.rate_plan == plano
    kmf_plano.fit(tenure[idx], observed[idx])
    kmf_plano.plot(ax=ax, legend=False)
    ax.annotate("Mediana =\n {:.0f} dias".format(kmf_plano.median_), xy = (.47, .5), 
                xycoords = "axes fraction")
    ax.set_xlabel("")
    ax.set_title(plano)
    ax.set_xlim(0,5000)
    ax.set_ylim(0,1)

fig.tight_layout()

fig.text(0.5, -0.01, "Timeline (Dias)", ha="center")

fig.text(-0.05, 0.5, "Probabilidade de que um cliente\n ainda esteja inscrito", 
         va="center", rotation="vertical")

fig.suptitle("Curva de sobrevivência de inscrições por tipo de plano (1958-2006)\n para clientes com até 5000 dias de inscrição",
             fontsize=20)

fig.subplots_adjust(top=0.7)

plt.show()


# In[ ]:


kmf_cidade = KaplanMeierFitter()

tenure = customers5000.tenure
observed = customers5000.inactive

planos = ["Metropolis", "Smallville", "Gotham"]

fig, axes = plt.subplots(nrows = 1, ncols=3, sharey = True, figsize=(10,5))

for plano, ax in zip(planos, axes.flatten()):
    idx = customers5000.market == plano
    kmf_cidade.fit(tenure[idx], observed[idx])
    kmf_cidade.plot(ax=ax, legend=False)
    ax.annotate("Mediana =\n {:.0f} dias".format(kmf_cidade.median_), xy = (.47, .5), 
                xycoords = "axes fraction")
    ax.set_xlabel("")
    ax.set_title(plano)
    ax.set_xlim(0,5000)
    ax.set_ylim(0,1)

fig.tight_layout()

fig.text(0.5, -0.01, "Timeline (Dias)", ha="center")

fig.text(-0.05, 0.5, "Probabilidade de que um cliente\n ainda esteja inscrito", 
         va="center", rotation="vertical")

fig.suptitle("Curva de sobrevivência de inscrições por mercado (1958-2006)\n para clientes com até 5000 dias de inscrição",
             fontsize=20)

fig.subplots_adjust(top=0.7)

plt.show()


# In[ ]:


kmf_canal = KaplanMeierFitter()

tenure = customers5000.tenure
observed = customers5000.inactive

planos = ["Mail", "Store", "Chain", "Dealer"]

fig, axes = plt.subplots(nrows = 1, ncols=4, sharey = True, figsize=(10,5))

for plano, ax in zip(planos, axes.flatten()):
    idx = customers5000.channel == plano
    kmf_canal.fit(tenure[idx], observed[idx])
    kmf_canal.plot(ax=ax, legend=False)
    ax.annotate("Mediana =\n {:.0f} dias".format(kmf_canal.median_), xy = (.47, .5), 
                xycoords = "axes fraction")
    ax.set_xlabel("")
    ax.set_title(plano)
    ax.set_xlim(0,5000)
    ax.set_ylim(0,1)

fig.tight_layout()

fig.text(0.5, -0.01, "Timeline (Dias)", ha="center")

fig.text(-0.05, 0.5, "Probabilidade de que um cliente\n ainda esteja inscrito", 
         va="center", rotation="vertical")

fig.suptitle("Curva de sobrevivência de inscrições por canal (1958-2006)\n para clientes com até 5000 dias de inscrição",
             fontsize=20)

fig.subplots_adjust(top=0.7)

plt.show()

