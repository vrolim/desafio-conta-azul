/* SELECT para aplicar máscara ao resultado */
SELECT divideEmCincoGrupos.costumerid, clients.firstname,
	recency * 100 + frequency * 10 + monetary as RFM

FROM clients, (
	/* SELECT para a divisão em 5 grupos iguais 
	(considerando Birant, Derya. "Data mining using RFM analysis." Knowledge-oriented applications in data mining. InTech, 2011.)*/
	SELECT costumerid,
		ntile(5) over (order by ultimoPedido) as recency,
		ntile(5) over (order by contPedidos) as frequency,
		ntile(5) over (order by mediaPreco) as monetary
	FROM(
											
		/* SELECT para agregação de dados por cliente. 
		São desconsiderados pedidos para o cliente com id 0, pois esse cliente não está disponível na base disponibilizada.
		Acredito que essa id 0 seja utilizada para compras em que o cliente não quis se identificar.
		Como existem muitos registros para essa id, sua utilização na análise implicaria na distorção dos resultados.
		*/
		SELECT costumerid,
			max(orderdate) as ultimoPedido,
			count(*) as contPedidos,
			avg(totalprice) as mediaPreco
		FROM orders
			WHERE costumerid > 0
			GROUP BY costumerid
			) agregaPorCliente
	)divideEmCincoGrupos
	WHERE
		clients.costumerid = divideEmCincoGrupos.costumerid
ORDER BY RFM DESC