CREATE OR REPLACE FUNCTION public.ultimoDiaMes(date)
RETURNS date AS
$body$
begin
    return (to_char(($1 + interval '1 month'),'YYYY-MM') || '-01')::date - 1;
end;
$body$
LANGUAGE 'plpgsql'



CREATE OR REPLACE FUNCTION public.primeiroAniversario(data date)
RETURNS TABLE(count bigint) AS
$body$
begin
    return QUERY SELECT COUNT(id) FROM clientes1ano
				WHERE
				data_assinatura BETWEEN data AND ultimoDiaMes(data);
end;
$body$
LANGUAGE 'plpgsql'

	
	
SELECT * FROM primeiroAniversario('2015-01-01')