SELECT caracter,  COUNT(*) as total_ocorrencias,
	SUM(CASE WHEN pos = 1 THEN 1 ELSE 0 END) as posição1,
	SUM(CASE WHEN pos = 2 THEN 1 ELSE 0 END) as posição2

FROM((SELECT SUBSTRING(palavra, 1, 1) as caracter, 1 as pos FROM caracteres)
	  UNION ALL
	  (SELECT SUBSTRING(palavra, 2, 1) as caracter, 2 as pos FROM caracteres)
	 )uniao_caracteres
GROUP BY caracter
ORDER BY caracter