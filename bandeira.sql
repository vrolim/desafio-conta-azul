SELECT binnumber,
	CASE
		WHEN CAST(LEFT(binnumber::text,1) AS INT) = 4 THEN
			'Visa'
	
		WHEN CAST(LEFT(binnumber::text,2) AS INT) IN (34, 37) THEN
			'American Express'
		
		WHEN CAST(LEFT(binnumber::text,2) AS INT) IN (36, 38, 39) THEN
			'Diners Club'
		
		WHEN CAST(LEFT(binnumber::text,2) AS INT) IN (51, 52, 53) THEN
			'Master Card'
	
		WHEN CAST(LEFT(binnumber::text,2) AS INT) IN (54, 55) THEN
			'Diners Club ou Master Card'
		
		WHEN CAST(LEFT(binnumber::text,3) AS INT) IN (300, 301, 302, 303, 304, 305, 309) THEN
			'Diners Club'
		
		ELSE
			'Outros'
	END
FROM cartoes;