
# coding: utf-8

# In[1]:


get_ipython().run_line_magic('matplotlib', 'inline')

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

plt.style.use('ggplot')


sns.set(palette = "Paired", font_scale = 1.35, 
        rc = {"figure.figsize": (5,5), "axes.facecolor": ".92"})


# In[2]:


veiculos = pd.read_csv("dados/veiculos.csv", na_values=[' '], dtype={'idade':float})


# In[3]:


veiculos.head()


# In[4]:



ax = veiculos['modelo'].value_counts().plot(kind='pie', autopct='%1.1f%%',colormap='Pastel2',fontsize=12);
plt.title('Resposta dos clientes para a pergunta:\n Qual o modelo do seu veículo?')
plt.xlabel('')
plt.ylabel('')

plt.show()


# In[159]:


ax = veiculos['opcionais'].value_counts().plot(kind='pie', autopct='%1.1f%%',colormap='Pastel2',fontsize=12);

plt.title('Resposta dos clientes para a pergunta:\n Qual opcional está instalado em seu veículo?')
plt.xlabel('')
plt.ylabel('')

plt.show()


# In[6]:


ax = veiculos['design'].value_counts().plot(kind='pie', autopct='%1.1f%%',colormap='Pastel2',fontsize=12);
plt.title('Resposta dos clientes para a pergunta:\n Qual sua opinião com relação ao design dos veículos?')
plt.xlabel('')
plt.ylabel('')

plt.show()


# In[7]:


ax = veiculos['concessionaria'].value_counts().plot(kind='pie', autopct='%1.1f%%',colormap='Pastel2',fontsize=12);
plt.title('Resposta dos clientes para a pergunta:\n Qual sua opinião com relação à concessionária onde compro o veículo?')
plt.xlabel('')
plt.ylabel('')

plt.show()


# In[8]:


ax = veiculos['geral'].value_counts().plot(kind='pie', autopct='%1.1f%%',colormap='Pastel2',fontsize=15);
plt.title('Resposta dos clientes para a pergunta:\n Qual sua opinião ao seu veículo, em geral?')
plt.xlabel('')
plt.ylabel('')

plt.show()


# In[160]:


ax = veiculos['idade']

bins_idade = [-float('inf'),25,35,40,float('inf')]

g1 = []
g2 = []
g3 = []
g4 = []
for a in ax:
    if a < 18:
        g4.append(a)
    elif a <= 25:
        g1.append(a)
    elif a <= 35:
        g2.append(a)
    elif a > 35 and a <=45:
        g3.append(a)
    else:
        g4.append(a)
        
idade = pd.DataFrame({'Até 25 anos': len(g1), 'De 25 a 35 anos': len(g2), 'De 35 a 45 anos':len(g3), 'Mais de 45 anos': len(g4)}, index=['coluna'])

i = idade.transpose()
i.plot(kind='pie', autopct='%1.1f%%',colormap='Pastel2',fontsize=15, y='coluna')
plt.legend().remove()
plt.title('Resposta dos clientes para a pergunta:\n Qual sua idade?')
plt.xlabel('')
plt.ylabel('')

plt.show()


# In[10]:


ax = veiculos['renda']

g1 = []
g2 = []
g3 = []
g4 = []
for a in ax:
    if a <= 10:
        g1.append(a)
    elif a > 10 and a <= 20:
        g2.append(a)
    elif a > 20 and a <=30:
        g3.append(a)
    elif a > 30:
        g4.append(a)
        
renda = pd.DataFrame({'Até 5 salários': len(g1), 'Entre 5 e 10 salários': len(g2), 'Entre 10 e 20 salários':len(g3), 'Mais que 20 salários':len(g4)},  index=['coluna'])
r = renda.transpose()
r.plot(kind='pie', autopct='%1.1f%%',colormap='Pastel2',fontsize=12, y='coluna')
plt.legend().remove()
plt.title('Resposta dos clientes para a pergunta:\n Qual sua renda, em salários?')
plt.xlabel('')
plt.ylabel('')

plt.show()


# In[161]:


ax = veiculos['remodelacao']
total = 0

g1 = []
g2 = []
g3 = []
g4 = [] 
g5 = []

for a in ax:
    if a == 1 or a == 0:
        g1.append(a)
    elif a == 2:
        g2.append(a)
    elif a == 3:
        g3.append(a)
    elif a == 4:
        g4.append(a)
    else:
        g5.append(a)
    total += 1
        
remod = pd.DataFrame({'Até 1 ano': len(g1), 'De 1 a 2 anos': len(g2), 'De 2 a 3 anos':len(g3), 
                      'De 3 a 4 anos':len(g4), 'Mais de 4 anos':len(g5)}, index=['coluna'])
r = remod.transpose()
explode = (0, 0, 0.1, 0.3, 0.4)
r.plot(kind='pie', autopct='%1.1f%%',colormap='Pastel2',fontsize=12, y='coluna', explode = explode)
plt.legend().remove()
plt.title('Resposta dos clientes para a pergunta:\n Qual sua percepção com relação à última vez que o veículo foi remodelado')
plt.xlabel('')
plt.ylabel('')
plt.axis('equal')

plt.show()



# In[12]:


ax = veiculos['pessoas']
total = 0

g1 = []
g2 = []
g3 = []
g4 = [] 
g5 = []
g6 = []

for a in ax:
    if a == 1 or a == 0:
        g1.append(a)
    elif a == 2:
        g2.append(a)
    elif a == 3:
        g3.append(a)
    elif a == 4:
        g4.append(a)
    elif a == 5:
        g5.append(a)
    else:
        g6.append(a)
    total += 1
        
pessoas = pd.DataFrame({'1': len(g1), '2': len(g2), '3':len(g3), '4':len(g4), '5':len(g5), '6':len(g6)}, index=['coluna'])
p = pessoas.transpose()
p.plot(kind='pie', autopct='%1.1f%%',colormap='Pastel2',fontsize=12, y='coluna')
plt.legend().remove()
plt.title('Resposta dos clientes para a pergunta:\n Quantas pessoas geralmente são transportadas no veículo?')
plt.xlabel('')
plt.ylabel('')
plt.axis('equal')

plt.show()


# In[162]:


ax = veiculos['km']
total = 0
300,400,500
g1 = []
g2 = []
g3 = []
g4 = []


for a in ax:
    if a <= 400:
        g1.append(a)
    elif a > 400 and a <= 500 :
        g2.append(a)
    elif a > 500 and a <= 600:
        g3.append(a)
    else:
        g4.append(a)
        

    total += 1
        
pessoas = pd.DataFrame({'Até 400 km': len(g1), 'Entre 400 e 500km': len(g2), 'Entre 500 e 600km':len(g3), 'Mais que 600km':len(g4)}, index=['coluna'])
p = pessoas.transpose()
p.plot(kind='pie', autopct='%1.1f%%',colormap='Pastel2',fontsize=12, y='coluna')
plt.legend().remove()
plt.title('Resposta dos clientes para a pergunta:\n Quantos quilômetros, em média, são percorridos em um mês com o veículo?')
plt.xlabel('')
plt.ylabel('')
plt.axis('equal')

plt.show()


# In[164]:


ax = veiculos.groupby(
    ['modelo', pd.cut(veiculos['idade'], bins=bins_idade, include_lowest=True)]
    )['modelo'].count().unstack('idade').fillna(0)
ax.plot(kind='bar', stacked=True, figsize=(7,7))
plt.title('Gráfico de aquisição de veículo por faixa etária')
plt.xlabel('Modelo')
plt.legend(['Até 25 anos', 'De 25 a 35 anos', 'De 35 a 45 anos', 'Mais de 45 anos'])


# In[52]:


ax = veiculos.groupby(
    ['pessoas', pd.cut(veiculos['idade'], bins=bins_idade, include_lowest=True)]
    )['pessoas'].count().unstack('idade').fillna(0)
ax.plot(kind='barh', stacked=True, figsize=(7,7))
plt.ylabel('Quantidade de pessoas transportadas')
plt.xlabel('')
plt.title('Gráfico de quantidade de pessoas transportadas por faixa etária')
plt.legend(['Até 25 anos', 'De 25 a 35 anos', 'De 35 a 45 anos', 'Mais de 45 anos'])



# In[169]:


ax = veiculos.groupby(
    [pd.cut(veiculos['renda'], bins=[-float('inf'), 5,10,20,float('inf')], include_lowest=True), pd.cut(veiculos['idade'], bins=bins_idade, include_lowest=True)]
    )['renda'].count().unstack('idade').fillna(0)
ax.plot(kind='bar', stacked=True, figsize=(7,7))
plt.ylabel('Renda mensal')
plt.xlabel('')
plt.title('Gráfico de renda mensal por faixa etária')
plt.legend(['Até 25 anos', 'De 25 a 35 anos', 'De 35 a 45 anos', 'Mais de 45 anos'])


# In[53]:


ax = veiculos.groupby(
    ['modelo', pd.cut(veiculos['pessoas'], bins=[1,2,3,4,5,6], include_lowest=True)]
    )['modelo'].count().unstack('pessoas').fillna(0)
ax.plot(kind='bar', stacked=True, figsize=(7,7))
plt.xlabel('Modelos de veículos')
plt.ylabel('')
plt.title('Gráfico de modelo de veículo por pessoas transportadas')
plt.legend(['1 pessoa', '2 pessoas', '3 pessoas', '4 pessoas', '5 pessoas', '6 pessoas'])


# In[156]:


ax = veiculos.groupby(
    ['modelo', pd.cut(veiculos['idade'], bins=bins_idade, include_lowest=True), 'geral']
    )['modelo'].count().unstack('modelo')

for a in ax:
    b = ax[a].unstack('idade')
    c = b.plot(kind='bar',figsize=(7,7), title=a)
    plt.legend(['Até 25 anos', 'De 25 a 35 anos', 'De 35 a 45 anos', 'Mais de 45 anos'])
    plt.xlabel('Percepção geral dos usuários com relação aos veículos')
    

plt.show()


# In[157]:


ax = veiculos.groupby(
    ['modelo', pd.cut(veiculos['idade'], bins=bins_idade, include_lowest=True), 'concessionaria']
    )['modelo'].count().unstack('modelo')

for a in ax:
    b = ax[a].unstack('idade')
    c = b.plot(kind='bar',figsize=(7,7), title=a)
    plt.legend(['Até 25 anos', 'De 25 a 35 anos', 'De 35 a 45 anos', 'Mais de 45 anos'])
    plt.xlabel('Percepção geral dos usuários com relação aos veículos')
    

plt.show()


# In[158]:


ax = veiculos.groupby(
    ['modelo', pd.cut(veiculos['idade'], bins=bins_idade, include_lowest=True), 'design']
    )['modelo'].count().unstack('modelo')

for a in ax:
    b = ax[a].unstack('idade')
    c = b.plot(kind='bar',figsize=(7,7), title=a)
    plt.legend(['Até 25 anos', 'De 25 a 35 anos', 'De 35 a 45 anos', 'Mais de 45 anos'])
    plt.xlabel('Percepção geral dos usuários com relação aos veículos')
    

plt.show()

